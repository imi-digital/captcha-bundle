<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use RapidData\CaptchaBundle\RapidDataCaptchaBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(RapidDataCaptchaBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
