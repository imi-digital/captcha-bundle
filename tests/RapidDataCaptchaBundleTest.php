<?php

declare(strict_types=1);

/*
 * This file is part of [package name].
 *
 * (c) John Doe
 *
 * @license LGPL-3.0-or-later
 */

namespace Contao\SkeletonBundle\Tests;

use PHPUnit\Framework\TestCase;
use RapidData\CaptchaBundle\RapidDataCaptchaBundle;

class RapidDataCaptchaBundleTest extends TestCase
{
    public function testCanBeInstantiated(): void
    {
        $bundle = new RapidDataCaptchaBundle();

        $this->assertInstanceOf('RapidData\CaptchaBundle\RapidDataCaptchaBundle', $bundle);
    }
}
